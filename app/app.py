from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'

db = SQLAlchemy(app)

class Todo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(200), unique=False, nullable=False)
    complete = db.Column(db.Boolean, unique=False, nullable=False)

    def __repr__(self):
    	return f"Todo {self.id}: {self.text}; Complete: {self.complete}"


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/new_todo', methods=['GET'])
def new_todo():
    error = None
    if request.method == 'GET':
        # add new todo
        return redirect('/', code=302)
    return render_template('index.html')

if __name__ == '__main__':
    app.run()
