from app import db

class Todo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(200), unique=False, nullable=False)
    complete = db.Column(db.Boolean, unique=False, nullable=False, default=False)

    def set_complete(self, complete):
        self.complete = complete

    def __repr__(self):
        return f"Todo {self.id}: {self.text}; Complete: {self.complete}"
