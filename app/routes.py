from app import app, db, logger
from flask import Flask, render_template, request, redirect
from app.models import Todo
from time import strftime

def load_todos():
    return Todo.query.all()

@app.route('/')
def index():
    return render_template('index.html', todos=load_todos())

@app.route('/new_todo', methods=['GET'])
def new_todo():
    # Log request
    error = None
    if request.method == 'GET':
        # check arguments
        todo = Todo(text=request.args.get("todo_text"), complete=False)
        db.session.add(todo)
        db.session.commit()
        app.logger.info("New Todo Success: Created Todo %(todo.id)d")
        return redirect('/', code=302)
    return render_template('index.html', todos=load_todos())

@app.route('/toggle_complete', methods=['GET'])
def toggle_complete():
    if request.method == 'GET':
        todo = Todo.query.filter_by(id=request.args.get("todo_id")).first()
        if todo is not None:
            todo.set_complete(not todo.complete)
            db.session.commit()
    return redirect('/', code=302)

@app.route('/delete_todo', methods=['GET'])
def delete_todo():
    if request.method == 'GET':
        todo = Todo.query.filter_by(id=request.args.get("todo_id")).first()
        if todo is not None:
            db.session.delete(todo)
            db.session.commit()
    return redirect('/', code=302)

@app.after_request
def after_request(response):
    timestamp = strftime('[%Y-%b-%d %H:%M]')
    logger.error('%s %s %s %s %s %s', timestamp, request.remote_addr, request.method, request.scheme, request.full_path, response.status)
    return response
