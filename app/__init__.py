from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate 

import logging
from logging.handlers import RotatingFileHandler

handler = RotatingFileHandler('logs/app.log', maxBytes=100000, backupCount=3)
logger = logging.getLogger('tdm')
logger.setLevel(logging.DEBUG)
logger.addHandler(handler)

db_handler = RotatingFileHandler('logs/sqlalchemy.log', maxBytes=100000, backupCount=3)
db_logger = logging.getLogger('sqlalchemy.engine')
db_logger.setLevel(logging.INFO)
db_logger.addHandler(db_handler)

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)

from app import routes, models
